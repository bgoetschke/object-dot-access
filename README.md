# Library to access nested object properties by providing a string

This library provides the class `BjoernGoetschke\ObjectDotAccess\ObjectAccessor` to read nested properties from
a `stdClass` object.

## Basic usage

Assume the following contents of a configuration file:

    {
        "services": {
            "database": {
                "host": "some_host",
                "username": "some_user",
                "password": "some_password"
            }
        }
    }

Read properties from a configuration file:

    $config = json_decode(file_get_contents('/path/to/configuration/file.json'));
    // assume that $config contains an object, should be checked in a real application
    $accessor = new BjoernGoetschke\ObjectDotAccess\ObjectAccessor($config);
    // read the database host from the configuration, or assume "localhost"
    $db_host = $accessor->getString('services.database.host', 'localhost');
    // read the database username from the configuration or throw an exception
    $db_user = $accessor->getStringOrError('services.database.user');
    // read the database password from the configuration or throw an exception
    $db_password = $accessor->getStringOrError('services.database.password');

The object accessor does not protect the used object from modification,
therefore the object accessor MUST NOT be considered as immutable.

## Installation

The library is available via Composer:

    composer require bjoern-goetschke/object-dot-access:^3.0

## Versioning

Releases will be numbered with the following format using semantic versioning:

`<major>.<minor>.<patch>`

And constructed with the following guidelines:

* Breaking backwards compatibility bumps the major
* New additions without breaking backwards compatibility bumps the minor
* Bug fixes and misc changes bump the patch

For more information on semantic versioning, please visit http://semver.org/.

## LICENSE

The library is released under the BSD-2-Clause license. You can find a copy of this license in LICENSE.txt.

## API usage and backwards compatibility

Information about the intended usage of interfaces, classes, methods, etc. is specified with the `@api` tag.

If an element does not contain the `@api` tag it should be considered internal and usage may break at any time.

One exception to this rule are special elements like constructors, destructors or other hook methods that are defined
by the programming language. These elements will not have their own `@api` tag but can be considered as if they have
the same `@api` tag as the class or whatever other element they belong to.

The library DOES NOT provide a backwards-compatibility promise for parameter names. Methods will have the
`@no-named-arguments` tag to help static analysis tools in detecting and warning about using the library with
named arguments, but in case it is missing somewhere that does not mean that a backwards-compatibility promise
is given for that specific method.

### `@api usage`

* Classes
    * Create new instances of the class
        * may break on `major`-releases
    * Extending the class and adding a new constructor
        * may break on `major`-releases
    * Extending the class and adding new methods
        * may break at any time, but `minor`-releases should be ok most of the time
          (will break if a non-private method has been added to the base class that was also declared
          in the extending class)
* Methods
    * Calling the method
        * may break on `major`-releases
    * Overriding the method (extending the class and declaring a method with the same name) and eventually
      adding additional optional arguments
        * may break at any time, but `minor`-releases should be ok most of the time
          (will break if an optional argument has been added to the method in the base-class)
* Interfaces
    * Using the interface in type-hints (require an instance of the interface as argument)
        * may break on `major`-releases
    * Calling methods of the interface
        * may break on `major`-releases
    * Implementing the interface
        * may break at any time, but `minor`-releases should be ok most of the time
          (will break if new methods have been added to the interface)
    * Extending the interface
        * may break at any time, but `minor`-releases should be ok most of the time
          (will break if a method has been added to the base interface that was also declared
          in the extending interface)

### `@api extend`

* Classes
    * Create new instances of the class
        * may break on `major`-releases
    * Extending the class and adding a new constructor
        * may break on `major`-releases
    * Extending the class and adding new methods
        * may break on `minor`-releases, but it should be ok most of the time and may only break on `major`-releases
          (will break if a non-private method has been added to the base class that was also declared in the
          extending class)
* Methods
    * Calling the method
        * may break on `major`-releases
    * Overriding the method (extending the class and declaring a method with the same name) and eventually
      adding additional optional arguments
        * may break on `major`-releases
* Interfaces
    * Using the interface in type-hints (require an instance of the interface as argument)
        * may break on `major`-releases
    * Calling methods of the interface
        * may break on `major`-releases
    * Implementing the interface
        * may break on `major`-releases
    * Extending the interface
        * may break on `major`-releases

### `@api stable`

* Everything that is marked as stable may only break on `major`-releases, this means that except some minor internal
  changes or bugfixes the code will just never change at all

### `@api internal`

* Everything that is marked as internal may break at any time, but `patch`-releases should be ok most of the time

<?php

declare(strict_types=1);

namespace BjoernGoetschke\ObjectDotAccess;

use BadMethodCallException;
use RuntimeException;
use stdClass;

/**
 * Provides access to nested object properties by providing a string.
 *
 * The internal structure of the root object is not protected and can be modified when accessing the root object
 * using {@see ObjectAccessor::getRoot()} or any nested object that is returned from the accessor methods.
 *
 * Therefore, this class MUST NOT be considered as immutable.
 *
 * @api usage
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class ObjectAccessor
{
    /**
     * The root object.
     */
    private stdClass $object;

    /**
     * Constructor.
     *
     * @param stdClass $object
     *        The root object whose attributes will be accessed.
     * @no-named-arguments
     */
    public function __construct(stdClass $object)
    {
        $this->object = $object;
    }

    /**
     * Prevent clone.
     *
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    /**
     * Prevent serialize.
     *
     * @return array<string, mixed>
     * @codeCoverageIgnore
     */
    public function __serialize(): array
    {
        throw new BadMethodCallException('Cannot serialize ' . __CLASS__);
    }

    /**
     * Prevent unserialize.
     *
     * @param array<string, mixed> $data
     * @codeCoverageIgnore
     */
    public function __unserialize(array $data): void
    {
        throw new BadMethodCallException('Cannot unserialize ' . __CLASS__);
    }

    /**
     * Create an object accessor from the specified {@see stdClass} object, or from an empty root object if
     * the specified value is not an instance of {@see stdClass}.
     *
     * @param mixed $value
     *        The root object whose attributes will be accessed.
     * @return self
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public static function fromObjectOrEmpty($value): self
    {
        if (!($value instanceof stdClass)) {
            $value = new stdClass();
        }

        return new self($value);
    }

    /**
     * Get the root object.
     *
     * @return stdClass
     * @api usage
     * @since 1.0
     */
    public function getRoot(): stdClass
    {
        return $this->object;
    }

    /**
     * Try to read the value from the specified path, returns true on success, otherwise false.
     *
     * @param string $path
     *        The path to be accessed.
     * @param mixed $value
     *        Will contain the value that is stored in the specified path, or null.
     * @return bool
     * @no-named-arguments
     */
    private function accessPath(string $path, &$value): bool
    {
        $value = null;
        $object = $this->object;
        $path = explode('.', $path);

        while ($object instanceof stdClass) {
            $part = array_shift($path);

            if (!property_exists($object, $part)) {
                return false;
            }

            if (count($path) < 1) {
                $value = $object->$part;
                return true;
            }

            $object = $object->$part;
        }

        return false;
    }

    /**
     * Returns true if the specified value is a base type, otherwise false.
     *
     * @param mixed $value
     *        The value that should be checked.
     * @return bool
     * @no-named-arguments
     */
    private function isBaseType(&$value): bool
    {
        return is_string($value) ||
            is_bool($value) ||
            is_int($value) ||
            is_float($value) ||
            is_null($value);
    }

    /**
     * Returns true if the specified path can be accessed, otherwise false.
     *
     * @param string $path
     *        The path to be accessed.
     * @return bool
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function has(string $path): bool
    {
        $value = null;
        return $this->accessPath($path, $value);
    }

    /**
     * Returns the value of the specified path, or the default value if the path can not be accessed.
     *
     * @param string $path
     *        The path to be accessed.
     * @param mixed $default
     *        The default value that will be returned in case the path could not be accessed.
     * @return mixed
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function get(string $path, $default = null)
    {
        $value = null;

        if (!$this->accessPath($path, $value)) {
            return $default;
        }

        return $value;
    }

    /**
     * Returns the value of the specified path, or throws a {@see RuntimeException} if the path can not be accessed.
     *
     * @param string $path
     *        The path to be accessed.
     * @return mixed
     * @throws RuntimeException
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function getOrError(string $path)
    {
        $value = null;

        if (!$this->accessPath($path, $value)) {
            $msg = sprintf(
                'Value "%1$s" not found.',
                $path,
            );
            throw new RuntimeException($msg);
        }

        return $value;
    }

    /**
     * Returns the value of the specified path as boolean, or the default value if the path can not be accessed.
     *
     * @param string $path
     *        The path to be accessed.
     * @param bool $default
     *        The default value that will be returned in case the path could not be accessed.
     * @return bool
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function getBoolean(string $path, bool $default = false): bool
    {
        $value = null;

        if (!$this->accessPath($path, $value) || !$this->isBaseType($value)) {
            return $default;
        }

        return (bool)$value;
    }

    /**
     * Returns the value of the specified path as boolean, or throws a {@see RuntimeException} if
     * the path can not be accessed.
     *
     * @param string $path
     *        The path to be accessed.
     * @return bool
     * @throws RuntimeException
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function getBooleanOrError(string $path): bool
    {
        $value = null;

        if (!$this->accessPath($path, $value) || !$this->isBaseType($value)) {
            $msg = sprintf(
                'Value "%1$s" not found or is not a boolean.',
                $path,
            );
            throw new RuntimeException($msg);
        }

        return (bool)$value;
    }

    /**
     * Returns the value of the specified path as integer, or the default value if the path can not be accessed.
     *
     * @param string $path
     *        The path to be accessed.
     * @param int $default
     *        The default value that will be returned in case the path could not be accessed.
     * @return int
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function getInteger(string $path, int $default = 0): int
    {
        $value = null;

        if (!$this->accessPath($path, $value) || !$this->isBaseType($value)) {
            return $default;
        }

        return (int)$value;
    }

    /**
     * Returns the value of the specified path as integer, or throws a {@see RuntimeException} if
     * the path can not be accessed.
     *
     * @param string $path
     *        The path to be accessed.
     * @return int
     * @throws RuntimeException
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function getIntegerOrError(string $path): int
    {
        $value = null;

        if (!$this->accessPath($path, $value) || !$this->isBaseType($value)) {
            $msg = sprintf(
                'Value "%1$s" not found or is not an integer.',
                $path,
            );
            throw new RuntimeException($msg);
        }

        return (int)$value;
    }

    /**
     * Returns the value of the specified path as float, or the default value if the path can not be accessed.
     *
     * @param string $path
     *        The path to be accessed.
     * @param float $default
     *        The default value that will be returned in case the path could not be accessed.
     * @return float
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function getFloat(string $path, float $default = 0.0): float
    {
        $value = null;

        if (!$this->accessPath($path, $value) || !$this->isBaseType($value)) {
            return $default;
        }

        return (float)$value;
    }

    /**
     * Returns the value of the specified path as float, or throws a {@see RuntimeException} if
     * the path can not be accessed.
     *
     * @param string $path
     *        The path to be accessed.
     * @return float
     * @throws RuntimeException
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function getFloatOrError(string $path): float
    {
        $value = null;

        if (!$this->accessPath($path, $value) || !$this->isBaseType($value)) {
            $msg = sprintf(
                'Value "%1$s" not found or is not a float.',
                $path,
            );
            throw new RuntimeException($msg);
        }

        return (float)$value;
    }

    /**
     * Returns the value of the specified path as string, or the default value if the path can not be accessed.
     *
     * @param string $path
     *        The path to be accessed.
     * @param string $default
     *        The default value that will be returned in case the path could not be accessed.
     * @return string
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function getString(string $path, string $default = ''): string
    {
        $value = null;

        if (!$this->accessPath($path, $value) || !$this->isBaseType($value)) {
            return $default;
        }

        return (string)$value;
    }

    /**
     * Returns the value of the specified path as string, or throws a {@see RuntimeException} if
     * the path can not be accessed.
     *
     * @param string $path
     *        The path to be accessed.
     * @return string
     * @throws RuntimeException
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function getStringOrError(string $path): string
    {
        $value = null;

        if (!$this->accessPath($path, $value) || !$this->isBaseType($value)) {
            $msg = sprintf(
                'Value "%1$s" not found or is not a string.',
                $path,
            );
            throw new RuntimeException($msg);
        }

        return (string)$value;
    }

    /**
     * Returns the value of the specified path as array, or the default value if the path can not be accessed.
     *
     * @param string $path
     *        The path to be accessed.
     * @param mixed[] $default
     *        The default value that will be returned in case the path could not be accessed.
     * @return mixed[]
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function getArray(string $path, array $default = []): array
    {
        /** @var mixed $value */
        $value = null;

        if (!$this->accessPath($path, $value) || !is_array($value)) {
            return $default;
        }

        return $value;
    }

    /**
     * Returns the value of the specified path as array, or throws a {@see RuntimeException} if
     * the path can not be accessed.
     *
     * @param string $path
     *        The path to be accessed.
     * @return mixed[]
     * @throws RuntimeException
     * @no-named-arguments
     * @api usage
     * @since 1.0
     */
    public function getArrayOrError(string $path): array
    {
        /** @var mixed $value */
        $value = null;

        if (!$this->accessPath($path, $value) || !is_array($value)) {
            $msg = sprintf(
                'Value "%1$s" not found or is not an array.',
                $path,
            );
            throw new RuntimeException($msg);
        }

        return $value;
    }

    /**
     * Flatten the root object to a one-dimensional array with the path as keys.
     *
     * Will traverse all {@see stdClass} values to generate the path, any other value
     * encountered will be used as value for the path that has led to this element.
     *
     * @param string $prefix
     *        Prefix to prepend before all keys.
     * @return array<string, mixed>
     * @no-named-arguments
     * @api usage
     * @since 1.1
     */
    public function flattenToArray(string $prefix = ''): array
    {
        return self::flattenObject($this->object, $prefix);
    }

    /**
     * Flatten the specified object to a one-dimensional array with the path as keys.
     *
     * Will traverse all {@see stdClass} values to generate the path, any other value
     * encountered will be used as value for the path that has led to this element.
     *
     * @param stdClass $object
     *        The object that should be flattened.
     * @param string $prefix
     *        Prefix to prepend before all keys.
     * @return array<string, mixed>
     * @no-named-arguments
     * @api usage
     * @since 2.0
     */
    public static function flattenObject(stdClass $object, string $prefix = ''): array
    {
        $return = [];

        foreach (get_object_vars($object) as $key => $value) {
            $path = (strlen($prefix) > 0) ? $prefix . '.' . $key : $key;
            if ($value instanceof stdClass) {
                $return = array_merge($return, self::flattenObject($value, $path));
            } else {
                $return[$path] = $value;
            }
        }

        return $return;
    }
}

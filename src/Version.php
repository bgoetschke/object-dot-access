<?php

declare(strict_types=1);

namespace BjoernGoetschke\ObjectDotAccess;

/**
 * Provides methods to check the library version.
 *
 * @api stable
 * @since 1.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class Version
{
    /**
     * Library version.
     *
     * @api stable
     * @since 1.0
     * @var string
     */
    public const VERSION = '3.0.2';

    /**
     * Private constructor.
     *
     * @codeCoverageIgnore
     */
    private function __construct()
    {
    }

    /**
     * Compare the specified version with the current version of the library.
     *
     * Compare the specified version string $version with the current value of {@see Version::VERSION}.
     *
     * Return value will be -1 if $version is older, 0 if they are the same and +1 if $version is newer.
     *
     * @param string $version
     *        The version string that should be compared to the current version of the library.
     * @return int
     * @no-named-arguments
     * @api stable
     * @since 1.0
     */
    public static function compareVersion(string $version): int
    {
        return version_compare(strtolower($version), strtolower(self::VERSION));
    }
}

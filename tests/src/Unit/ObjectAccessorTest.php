<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\ObjectDotAccess\Unit;

use BjoernGoetschke\ObjectDotAccess\ObjectAccessor;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use stdClass;

final class ObjectAccessorTest extends TestCase
{
    private static function buildTestObject(): stdClass
    {
        return json_decode(
            '
            {
                "integer":
                {
                    "value": 42
                },
                "float":
                {
                    "value": 1337.42
                },
                "string":
                {
                    "value": "someString",
                    "integer": "42",
                    "float": "1337.42",
                    "zero": "0",
                    "one": "1"
                },
                "boolean":
                {
                    "true": true,
                    "false": false
                },
                "some":
                {
                    "deep":
                    {
                        "nested":
                        {
                            "array":
                            [
                                "value1",
                                "value2"
                            ]
                        }
                    }
                }
            }
        ',
        );
    }

    public function testFromObjectOrEmptyStdClassIsUsed(): void
    {
        $object = new stdClass();

        $accessor = ObjectAccessor::fromObjectOrEmpty($object);

        self::assertSame(
            $object,
            $accessor->getRoot(),
        );
    }

    public function testFromObjectOrEmptyOtherValuesDiscarded(): void
    {
        $value1 = null;
        $accessor1 = ObjectAccessor::fromObjectOrEmpty($value1);

        self::assertCount(
            0,
            (array)$accessor1->getRoot(),
        );

        $value2 = 'someString';
        $accessor2 = ObjectAccessor::fromObjectOrEmpty($value2);

        self::assertCount(
            0,
            (array)$accessor2->getRoot(),
        );

        $value3 = 42;
        $accessor3 = ObjectAccessor::fromObjectOrEmpty($value3);

        self::assertCount(
            0,
            (array)$accessor3->getRoot(),
        );

        $value4 = 1337.42;
        $accessor4 = ObjectAccessor::fromObjectOrEmpty($value4);

        self::assertCount(
            0,
            (array)$accessor4->getRoot(),
        );

        $value5 = [
            'someKey' => 'someValue',
        ];
        $accessor5 = ObjectAccessor::fromObjectOrEmpty($value5);

        self::assertCount(
            0,
            (array)$accessor5->getRoot(),
        );
    }

    public function testHas(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());

        self::assertTrue(
            $accessor->has('integer.value'),
        );

        self::assertTrue(
            $accessor->has('float'),
        );

        self::assertTrue(
            $accessor->has('some.deep.nested.array'),
        );

        self::assertFalse(
            $accessor->has('float.does_not_exist'),
        );

        self::assertFalse(
            $accessor->has('string.value.does_not_exist'),
        );
    }

    public function testGet(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());
        $default = new stdClass();

        self::assertSame(
            42,
            $accessor->get('integer.value', $default),
        );

        self::assertSame(
            1337.42,
            $accessor->get('float.value', $default),
        );

        self::assertTrue(
            $accessor->get('boolean.true', $default),
        );

        self::assertSame(
            $default,
            $accessor->get('float.does_not_exist', $default),
        );

        self::assertSame(
            [
                'value1',
                'value2',
            ],
            $accessor->get('some.deep.nested.array', $default),
        );
    }

    public function testGetOrError(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());

        self::assertSame(
            42,
            $accessor->getOrError('integer.value'),
        );

        self::assertSame(
            1337.42,
            $accessor->getOrError('float.value'),
        );

        self::assertTrue(
            $accessor->getOrError('boolean.true'),
        );

        self::assertSame(
            [
                'value1',
                'value2',
            ],
            $accessor->getOrError('some.deep.nested.array'),
        );
    }

    public function testGetOrErrorThrowsException(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());

        $this->expectException(RuntimeException::class);

        $accessor->getOrError('float.does_not_exist');
    }

    public function testGetBoolean(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());

        self::assertTrue(
            $accessor->getBoolean('boolean.true', false),
        );

        self::assertFalse(
            $accessor->getBoolean('boolean.false', false),
        );

        self::assertTrue(
            $accessor->getBoolean('string.one', false),
        );

        self::assertFalse(
            $accessor->getBoolean('string.zero', false),
        );

        self::assertFalse(
            $accessor->getBoolean('float.does_not_exist', false),
        );
    }

    public function testGetBooleanOrError(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());

        self::assertTrue(
            $accessor->getBooleanOrError('boolean.true'),
        );

        self::assertFalse(
            $accessor->getBooleanOrError('boolean.false'),
        );

        self::assertTrue(
            $accessor->getBooleanOrError('string.one'),
        );

        self::assertFalse(
            $accessor->getBooleanOrError('string.zero'),
        );
    }

    public function testGetBooleanOrErrorNotExistingThrowsException(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());

        $this->expectException(RuntimeException::class);

        $accessor->getBooleanOrError('float.does_not_exist');
    }

    public function testGetBooleanOrErrorWrongTypeThrowsException(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());

        $this->expectException(RuntimeException::class);

        $accessor->getBooleanOrError('some.deep.nested.array');
    }

    public function testGetInteger(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());
        $default = 12345;

        self::assertSame(
            42,
            $accessor->getInteger('integer.value', $default),
        );

        self::assertSame(
            42,
            $accessor->getInteger('string.integer', $default),
        );

        self::assertSame(
            $default,
            $accessor->getInteger('float.does_not_exist', $default),
        );
    }

    public function testGetIntegerOrError(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());

        self::assertSame(
            42,
            $accessor->getIntegerOrError('integer.value'),
        );

        self::assertSame(
            42,
            $accessor->getIntegerOrError('string.integer'),
        );
    }

    public function testGetIntegerOrErrorNotExistingThrowsException(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());

        $this->expectException(RuntimeException::class);

        $accessor->getIntegerOrError('float.does_not_exist');
    }

    public function testGetIntegerOrErrorWrongTypeThrowsException(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());

        $this->expectException(RuntimeException::class);

        $accessor->getIntegerOrError('some.deep.nested.array');
    }

    public function testGetFloat(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());
        $default = 13.37;

        self::assertSame(
            1337.42,
            $accessor->getFloat('float.value', $default),
        );

        self::assertSame(
            1337.42,
            $accessor->getFloat('string.float', $default),
        );

        self::assertSame(
            $default,
            $accessor->getFloat('float.does_not_exist', $default),
        );
    }

    public function testGetFloatOrError(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());

        self::assertSame(
            1337.42,
            $accessor->getFloatOrError('float.value'),
        );

        self::assertSame(
            1337.42,
            $accessor->getFloatOrError('string.float'),
        );
    }

    public function testGetFloatOrErrorNotExistingThrowsException(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());

        $this->expectException(RuntimeException::class);

        $accessor->getFloatOrError('float.does_not_exist');
    }

    public function testGetFloatOrErrorWrongTypeThrowsException(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());

        $this->expectException(RuntimeException::class);

        $accessor->getFloatOrError('some.deep.nested.array');
    }

    public function testGetString(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());
        $default = 'defaultString';

        self::assertSame(
            'someString',
            $accessor->getString('string.value', $default),
        );

        self::assertSame(
            '42',
            $accessor->getString('integer.value', $default),
        );

        self::assertSame(
            $default,
            $accessor->getString('float.does_not_exist', $default),
        );
    }

    public function testGetStringOrError(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());

        self::assertSame(
            'someString',
            $accessor->getStringOrError('string.value'),
        );

        self::assertSame(
            '42',
            $accessor->getStringOrError('integer.value'),
        );
    }

    public function testGetStringOrErrorNotExistingThrowsException(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());

        $this->expectException(RuntimeException::class);

        $accessor->getStringOrError('float.does_not_exist');
    }

    public function testGetStringOrErrorWrongTypeThrowsException(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());

        $this->expectException(RuntimeException::class);

        $accessor->getStringOrError('some.deep.nested.array');
    }

    public function testGetArray(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());
        $default = [];

        self::assertSame(
            [
                'value1',
                'value2',
            ],
            $accessor->getArray('some.deep.nested.array'),
        );

        self::assertSame(
            $default,
            $accessor->getArray('float.does_not_exist', $default),
        );
    }

    public function testGetArrayOrError(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());

        self::assertSame(
            [
                'value1',
                'value2',
            ],
            $accessor->getArrayOrError('some.deep.nested.array'),
        );
    }

    public function testGetArrayOrErrorNotExistingThrowsException(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());

        $this->expectException(RuntimeException::class);

        $accessor->getArrayOrError('float.does_not_exist');
    }

    public function testGetArrayOrErrorWrongTypeThrowsException(): void
    {
        $accessor = new ObjectAccessor(self::buildTestObject());

        $this->expectException(RuntimeException::class);

        $accessor->getArrayOrError('string.value');
    }

    public function testFlattenToArray(): void
    {
        $object = (object)[
            'hello' => 'world',
            'answer' => 42,
        ];

        $accessor = new ObjectAccessor($object);

        self::assertSame(
            [
                'hello' => 'world',
                'answer' => 42,
            ],
            $accessor->flattenToArray(),
        );
    }

    public function testFlattenObjectBasic(): void
    {
        $object = (object)[
            'hello' => 'world',
            'answer' => 42,
        ];

        self::assertSame(
            [
                'hello' => 'world',
                'answer' => 42,
            ],
            ObjectAccessor::flattenObject($object),
        );
    }

    public function testFlattenObjectWithPrefix(): void
    {
        $object = (object)[
            'hello' => 'world',
            'answer' => 42,
        ];

        self::assertSame(
            [
                'test.hello' => 'world',
                'test.answer' => 42,
            ],
            ObjectAccessor::flattenObject($object, 'test'),
        );
    }

    public function testFlattenObjectComplex(): void
    {
        $object = self::buildTestObject();

        self::assertSame(
            [
                'test.integer.value' => 42,
                'test.float.value' => 1337.42,
                'test.string.value' => 'someString',
                'test.string.integer' => '42',
                'test.string.float' => '1337.42',
                'test.string.zero' => '0',
                'test.string.one' => '1',
                'test.boolean.true' => true,
                'test.boolean.false' => false,
                'test.some.deep.nested.array' => ['value1', 'value2'],
            ],
            ObjectAccessor::flattenObject($object, 'test'),
        );
    }
}

<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\ObjectDotAccess\Unit;

use BjoernGoetschke\ObjectDotAccess\Version;
use PHPUnit\Framework\TestCase;

final class VersionTest extends TestCase
{
    public function testCompareVersionOlder(): void
    {
        $testVersion = '0.' . Version::VERSION;
        $actual = Version::compareVersion($testVersion);
        $expected = -1;

        self::assertEquals($expected, $actual);
    }

    public function testCompareVersionEqual(): void
    {
        $testVersion = Version::VERSION;
        $actual = Version::compareVersion($testVersion);
        $expected = 0;

        self::assertEquals($expected, $actual);
    }

    public function testCompareVersionNewer(): void
    {
        $testVersion = Version::VERSION . '.1';
        $actual = Version::compareVersion($testVersion);
        $expected = 1;

        self::assertEquals($expected, $actual);
    }
}
